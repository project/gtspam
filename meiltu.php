<?php

/**
 * @file
 * This module implements a filter to obfuscate email addresses from spambots.  
 *
 * @{
------------------------------------------------------------
Author: GlobalTEK (http://globaltek.pt)
Description: GTSpam Module for Drupal
Version: $Revision$

Copyright (c) 2007 GlobalTEK

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Copy of GNU Lesser General Public License at: 
http://www.gnu.org/copyleft/lesser.txt

Contact the author at: diana.soares(_at_)globaltek.pt
------------------------------------------------------------
 * @}
 */


require_once './gtspam.module';
gtspam_meiltu(); 

?>
