------------------------------------------------------------
Author: GlobalTEK (http://globaltek.pt)
Description: GTSpam Module for Drupal
Version: $Revision$

Copyright (c) 2007 GlobalTEK

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Copy of GNU Lesser General Public License at: 
http://www.gnu.org/copyleft/lesser.txt

Contact the author at: diana.soares(_at_)globaltek.pt
------------------------------------------------------------


------
AUTHOR
------

GlobalTEK: http://globaltek.pt
Contact:   diana.soares(_at_)globaltek.pt


------
README
------

The GTSpam module obfuscates email addresses to help prevent spambots
from collecting them. It does that without loosing the mailto URL
scheme default semantics and operations (see RFC 2368).

Based on ideas from James Thornton.
in http://jamesthornton.com/software/graphic@.html


------------
INSTALLATION
------------

1. Extract the files (ok, you have already done that ;-). 

2. Copy/Move the extracted directory into your modules directory 
   (probably one of sites/*/modules/).

3. Enable the module. Go to your drupal web site in 'Administer >
   Modules', and enable the GTSpam module.

3. Enable the filter in the desired input formats. Go to 
   'Administer > Configuration > Input formats' for that.

4. Rearrange the filters on the input format's 'rearrange' tab to
   resolve conflicts with other filters.

5. Select the configure tab to configure the available settings. 
   Note that GTSpam settings are unique to all input formats. 
   YOU SHOULD CHANGE the default key used to encode the email 
   address to one of your own. 

